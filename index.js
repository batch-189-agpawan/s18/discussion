// Function parameters are variables that wait for a value from an arguement in the function invocation
function sayMyName(name){
	console.log('My name is ' + name)
}

// Function arguements are values like strings, numbers, etc. that you can pass unto the function you are invoking
sayMyName('Slim Shady')


// you can use re-use functions by invoking them at any time or any part of your code. Make sure you have declared them first
sayMyName('vice Ganda')
sayMyName('joferlyn')

// you can also pass variables as arguements to a function
let myName = 'enzo'
sayMyName(myName)


// You can use arguements and parameters to make the datya inside your function dynamic
function sumOfTwoNumbers(firstNumber , secondNumber){
	let sum = 0

	sum = firstNumber + secondNumber
	console.log(sum)
}

sumOfTwoNumbers(10,15)
sumOfTwoNumbers(99,1)
sumOfTwoNumbers(50,20)


// You can pass a function as an arguement for another function. Do Not pass that function with parentheses, only with its function name
function arguementFunction(){
	console.log('This is a function that was passed as an arguement')
}

function parameterFunction(arguementFunction){
	arguementFunction()
}

parameterFunction(arguementFunction)


// REAL WORLD APPLICATION
/*
	Imagine a product page where you have an 'add to cart' button wherein you have the button element itself displayed on the page BUT you don't have the functionality of it yet. This is where passing a function as an arguement comes in, in order for you to add or have access to a function to add functionality to your button once it is clicked
*/
function addProductToCart(){
	console.log('Click me to add product to cart')
}

let addToCartBtn = document.querySelector('#add-to-cart-btn')

addToCartBtn.addEventListener('click', addProductToCart)

// if you add an extra or if you lack an arguement, JS won't throw an error at you, instead it will ignore the extra arguement and turn the lacking parameter into undefined 
function displayFullname(firstName, middleInitial, lastName,extraText){
	console.log('Your full name is: ' + firstName+' ' + middleInitial +' '+ lastName + ' '+ extraText)
}

// displayFullname('Enzo', 'd', 'great')
displayFullname('johnny', 'd', 'goode', 'yeah')


// this is a string interpolation - instead of a regular concatination, we can interpolate or inject the variables within the string itself
// function displayMovieDetails(title, synopsis, director){
// 	console.log(`The movie is titled ${title}`)
// 	console.log(`The synopsis is ${synopsis}`)
// 	console.log(`The director is ${director}`)
// }

// displayMovieDetails('sisterakas','syniopao', 'wenn deramas')

// You can use double quotation for words with ''
// console.log('Congrats to the GSW')
// console.log("my money dont jigle jiggle")


// Return statements/syntax is the final step of any function. It asigns whatever value you put after it as the value of the function upon invoiking it.
/*
	NOTE:
	1. Return tatements should ALWAYS come last in the function
	2. Any statement after the RETURN statement will be ignored by the function
*/
function displayPlayerDetails(name, age, playerClass){
	// console.log(`Player name:${name}`)
	// console.log(`Player name:${age}`)
	// console.log(`Player name:${playerClass}`)
	let playerDetails = `Name: ${name}, Age: ${age}, Class:${playerClass}`

	return playerDetails
}

console.log(displayPlayerDetails('elsworth', 120, 'paladin'))